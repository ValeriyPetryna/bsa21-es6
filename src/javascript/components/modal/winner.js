import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  showModal({
    title: `${fighter.name} win!`,
    bodyElement: createFighterImage(fighter),
  });
}
