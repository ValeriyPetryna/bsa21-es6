import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} preview`,
  });

  if (fighter != undefined) {
    const fighterImage = createFighterImage(fighter);
    fighter.position = position;
    const fighterName = createElement({
      tagName: 'span',
      className: 'arena___fighter-name preview',
    });

    const fighterInfoBlock = createElement({
      tagName: 'div',
      className: 'fighter-preview___info preview',
    });

    fighterName.innerText = fighter.name;

    fighterInfoBlock.innerHTML = `
      <p> Health : ${fighter.health} </p>
      <p> Attack : ${fighter.attack} </p>
      <p> Defense : ${fighter.defense} </p>
      `;

    fighterElement.append(fighterName, fighterImage, fighterInfoBlock);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img preview',
    attributes,
  });

  return imgElement;
}
