import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    runOnKeys(firstFighter, secondFighter, resolve);
  });
}

export function getDamage(attacker, defender) {
  let power = getHitPower(attacker) - getBlockPower(defender);

  return power > 0 ? power : 0;
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;

  return power;
}

export const runOnKeys = (firstFighter, secondFighter, resolve) => {
  let pressed = new Set();
  let isWin = false;
  let firstFighterDoubleHit = true;
  let secondFighterDoubleHit = true;

  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;

  document.addEventListener('keydown', function (event) {
    pressed.add(event.code);
  });
  document.addEventListener('keyup', function (event) {
    let damage = 0;

    if (isFirstSuperHit(pressed) && firstFighterDoubleHit) {
      firstFighterDoubleHit = false;
      damage = getDoubleHit(firstFighter);

      getHit(secondFighter, damage);
      setTimeout(() => (firstFighterDoubleHit = true), 10000);

      pressed.clear();
    }

    if (isSecondSuperHit(pressed) && secondFighterDoubleHit) {
      secondFighterDoubleHit = false;
      damage = getDoubleHit(firstFighter);

      getHit(firstFighter, damage);
      setTimeout(() => (secondFighterDoubleHit = true), 10000);

      pressed.clear();
    }

    if (firstPlayerHit(pressed) && !someOneBlock(pressed)) {
      damage = getDamage(firstFighter, secondFighter);
      getHit(secondFighter, damage);
    }

    if (secondPlayerHit(pressed) && !someOneBlock(pressed)) {
      damage = getDamage(secondFighter, firstFighter);
      getHit(firstFighter, damage);
    }

    isWin = checkWinner(firstFighter, secondFighter);
    isWin ? resolve(isWin) : pressed.delete(event.code);
  });
};

const getHit = (punchedFighter, damage) => {
  punchedFighter.currentHealth -= damage;
  changeHealthBar(punchedFighter);
};

const getDoubleHit = (fighter) => {
  return fighter.attack * 2;
};

const someOneBlock = (pressed) => {
  return pressed.has(controls.PlayerOneBlock) || pressed.has(controls.PlayerTwoBlock) ? true : false;
};

const firstPlayerHit = (pressed) => {
  return pressed.has(controls.PlayerOneAttack);
};

const isFirstSuperHit = (pressed) => {
  return controls.PlayerOneCriticalHitCombination.every((key) => pressed.has(key));
};

const isSecondSuperHit = (pressed) => {
  return controls.PlayerTwoCriticalHitCombination.every((key) => pressed.has(key));
};

const secondPlayerHit = (pressed) => {
  return pressed.has(controls.PlayerTwoAttack);
};

const changeHealthBar = (fighter) => {
  const barStatus = (fighter.currentHealth * 100) / fighter.health;
  fighter.position == 'left'
    ? (document.getElementById('left-fighter-indicator').style.width = `${barStatus > 0 ? barStatus : 0}%`)
    : (document.getElementById('right-fighter-indicator').style.width = `${barStatus > 0 ? barStatus : 0}%`);
};

const checkWinner = (firstFighter, secondFighter) => {
  if (firstFighter.currentHealth <= 0) {
    return secondFighter;
  } else if (secondFighter.currentHealth <= 0) {
    return firstFighter;
  } else return false;
};
